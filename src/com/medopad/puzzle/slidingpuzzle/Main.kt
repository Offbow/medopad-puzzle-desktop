package com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import com.medopad.puzzle.slidingpuzzle.solver.BreadthFirstSearch

val solverBlocks = listOf(
    Block(Block.WALL_ID, 1, 6, 0, 0, 'X', Block.BlockType.WALL),
    Block(Block.WALL_ID, 6, 1, 0, 1, 'X', Block.BlockType.WALL),
    Block(Block.WALL_ID, 6, 1, 5, 1, 'X', Block.BlockType.WALL),
    Block(Block.WALL_ID, 1, 1, 1, 6, 'X', Block.BlockType.WALL),
    Block(Block.WALL_ID, 1, 1, 4, 6, 'X', Block.BlockType.WALL),
    Block(Block.GOAL_ID, 1, 2, 2, 6, 'Z', Block.BlockType.WALL_GOAL),
    Block(3, 2, 1, 1, 1, 'a', Block.BlockType.PIECE),
    Block(4, 2, 1, 4, 1, 'c', Block.BlockType.PIECE),
    Block(5, 2, 1, 1, 3, 'd', Block.BlockType.PIECE),
    Block(6, 2, 1, 4, 3, 'f', Block.BlockType.PIECE),
    Block(7, 1, 2, 2, 3, 'e', Block.BlockType.PIECE),
    Block(8, 1, 1, 2, 4, 'g', Block.BlockType.PIECE),
    Block(9, 1, 1, 3, 4, 'h', Block.BlockType.PIECE),
    Block(10, 1, 1, 1, 5, 'i', Block.BlockType.PIECE),
    Block(11, 1, 1, 4, 5, 'j', Block.BlockType.PIECE),
    Block(12, 2, 2, 2, 1, 'b', Block.BlockType.PIECE_WIN)
)

fun main() {
    val startTime = System.currentTimeMillis()
    val breadthFirstSearch = BreadthFirstSearch()
    println("Started")
    val solution = breadthFirstSearch.findSolutionAllPermutations(solverBlocks).minBy { it.size }!!
//    val solution = breadthFirstSearch.findSolution(solverBlocks)
    println("Solutions founds ${solution.size}")
    solution.forEach { node ->
        println(BoardMapUtil.formatBoardMap(node.getMap()))
    }
    println("Time Taken ${System.currentTimeMillis() - startTime}ms")

}