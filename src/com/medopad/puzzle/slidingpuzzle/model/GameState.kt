package com.medopad.puzzle.slidingpuzzle.model

sealed class GameState()

class GameWonState(val totalMoves: Int) : GameState()
class GameProgressState(val currentMoves: Int) : GameState()
class GameNoMovementState(val currentMoves: Int) : GameState()
class GameInvalidMovementState(val currentMoves: Int) : GameState()