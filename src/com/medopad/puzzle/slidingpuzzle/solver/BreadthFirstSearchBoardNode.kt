package com.medopad.puzzle.slidingpuzzle.solver

import com.medopad.puzzle.slidingpuzzle.Board
import com.medopad.puzzle.slidingpuzzle.Board.MoveResult.BLOCKED
import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.BLANK_ID
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.GOAL_ID
import com.medopad.puzzle.slidingpuzzle.model.Block.Companion.WALL_ID
import com.medopad.puzzle.slidingpuzzle.model.GameMove

class BreadthFirstSearchBoardNode(blockList: List<Block>,
                                  val parentNode: BreadthFirstSearchBoardNode? = null,
                                  val boardNodeState: MoveResult? = null,
                                  val move: GameMove? = null,
                                  val distance: Int? = null,
                                  val blockID: Int? = null) : Board(blockList) {

    init {
        //On construction perform the block coordinate update
        if (move != null && distance != null && blockID != null) {
            updateBlockCoordinates(move, distance, blockList.first { it.blockId == blockID })
        }
    }

    /**
     * Return a list of all possible board/s where a valid move has been made.
     *
     * @param gameMovePermutation A set of game moves that each this node will use to expand
     * @return List<BreadthFirstSearchBoardNode> list of all possible child nodes.
     */
    fun expandNode(gameMoves: Array<GameMove>): List<BreadthFirstSearchBoardNode> {

        //List of pieces that can be moved (Not the walls, a blank space or the goal)
        //todo This could be predetermined
        val moveablePieces = currentGamePieces
                .filter { it.blockId != WALL_ID && it.blockId != BLANK_ID && it.blockId != GOAL_ID }

        val validMoveNodes = mutableListOf<BreadthFirstSearchBoardNode>()
        for (block in moveablePieces) {
            //todo implement options to change GameMove order for different results
            for (move in gameMoves) {
                for (distance in 1..2) {
                    val moveResult = canBlockMove(getDirectionCoordinates(move, distance, block.copy()), block.blockType)

                    if (moveResult != BLOCKED) {
                        //All possible moves added to returned list
                        validMoveNodes.add(BreadthFirstSearchBoardNode(currentGamePieces.toList().map { it.copy() }, this, moveResult, move, distance, block.blockId))
                    }
                }
            }
        }
        return validMoveNodes
    }

    /**
     * Returns a unique hash of the current nodes board layout.
     *
     * @return Int a unique hash of the board.
     */
    fun getBoardHash(): Int = occupiedSpaces.contentDeepHashCode()

}