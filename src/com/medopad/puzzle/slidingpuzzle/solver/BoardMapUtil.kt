package com.medopad.puzzle.slidingpuzzle.solver

import com.medopad.puzzle.slidingpuzzle.model.Block

object BoardMapUtil {
    fun formatBoardMap(map: List<List<Block?>>): String {
        var mapBuilder: String = ""
        for (array in map) {
            for (value in array) {
                if (value != null) {
                    mapBuilder += value.blockCharacter
                } else {
                    mapBuilder += '0'
                }
            }
            mapBuilder += "\n"
        }
        return mapBuilder
    }
}