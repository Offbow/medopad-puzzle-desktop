package com.medopad.puzzle.slidingpuzzle.solver

import com.medopad.puzzle.slidingpuzzle.Board
import com.medopad.puzzle.slidingpuzzle.model.Block
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.model.gameMovePermutations
import io.reactivex.Single
import java.util.*

class BreadthFirstSearch {

    /**
     * Returns a list of all moves needed to solve the board as per [blockList] and [gameMovePermutation].
     * NOTE: a list of length 0 means that a solution could not be found
     *
     * @param blockList A block list used to setup the root nodes
     * @param gameMovePermutation A set of game moves that each node will use to expand
     * @return List<BreadthFirstSearchBoardNode> An ordered of nodes.
     */
    fun findSolution(blockList: List<Block>, gameMovePermutation: Array<GameMove> = GameMove.values()): List<BreadthFirstSearchBoardNode> {
        //List of node hashes to make sure already included configuration are not duplicated
        val foundNodes = HashSet<Int>()
        //List of node to perform expansion on
        val queuedNodes = LinkedList<BreadthFirstSearchBoardNode>()

        val rootNode = BreadthFirstSearchBoardNode(blockList)
        queuedNodes.add(rootNode)

        var currentNodeHash: Int = rootNode.getBoardHash()
        foundNodes.add(currentNodeHash)

        var currentNode: BreadthFirstSearchBoardNode?

        while (queuedNodes.isNotEmpty()) {
            currentNode = queuedNodes.poll()
            if (currentNode == null) {
                continue
            }

            //Expand node and add all that are not already part of the search
            for (next in currentNode.expandNode(gameMovePermutation)) {
                currentNodeHash = next.getBoardHash()
                if (foundNodes.contains(currentNodeHash)) {
                    //Node already in found nodes
                    continue
                }
                foundNodes.add(currentNodeHash)
                queuedNodes.offer(next)
                if (next.boardNodeState == Board.MoveResult.WIN) {
                    return traceSolutionPath(next)
                }
            }
        }
        return emptyList()
    }

    /**
     * A simple wrapper using RxSingle to perform the [findSolution()] method.
     *
     * @return Single<List<BreadthFirstSearchBoardNode>> A RxSingle Wrapper for [findSolution()].
     */
    fun findSolutionAsSingle(blockList: List<Block>): Single<List<BreadthFirstSearchBoardNode>> {
        return Single.just(blockList)
                .map {
                    findSolution(it)
                }
    }
    /**
     * Returns lists of list's of all moves needed to solve the board as per [blockList].
     * This function simply performs the [findSolutionAllPermutations()] method with a set of all [GameMove] permutations
     * NOTE: a list of length 0 means that a solution could not be found
     *
     * @param blockList A block list used to setup the root nodes
     * @return List<List<BreadthFirstSearchBoardNode>>> A list of solutions list with ordered nodes.
     */
    fun findSolutionAllPermutations(blockList: List<Block>): List<List<BreadthFirstSearchBoardNode>> {
        val solutionList = mutableListOf<List<BreadthFirstSearchBoardNode>>()
        for (gameMoves in gameMovePermutations) {
            solutionList.add(findSolution(blockList, gameMoves))
        }
        return solutionList
    }

    /**
     * A simple wrapper using RxSingle to perform the [findSolutionAllPermutations()] method.
     *
     * @return Single<List<List<BreadthFirstSearchBoardNode>>> A RxSingle Wrapper for [findSolutionAllPermutations()].
     */
    fun findSolutionWithAllPermutationsAsSingle(blockList: List<Block>): Single<List<List<BreadthFirstSearchBoardNode>>> {
        return Single.just(blockList)
                .map {
                    findSolutionAllPermutations(it)
                }
    }

    private fun traceSolutionPath(node: BreadthFirstSearchBoardNode): List<BreadthFirstSearchBoardNode> {
        val solutionList: MutableList<BreadthFirstSearchBoardNode> = mutableListOf()
        var currentNode = node
        solutionList.add(currentNode)
        while (currentNode.parentNode != null) {
            currentNode = currentNode.parentNode!!
            solutionList.add(currentNode)
        }
        return solutionList.reversed()
    }

}