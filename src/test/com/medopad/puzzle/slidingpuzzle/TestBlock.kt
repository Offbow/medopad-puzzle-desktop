package test.com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.model.Block
import org.junit.Assert
import org.junit.Test

class TestBlock {

    @Test
    fun `Test Block adjacent Coordinate`() {
        //GIVEN
        val blockHeight = 2
        val blockWidth = 2
        val blockX = 4
        val blockY = 4
        val block = Block(99, blockHeight, blockWidth, blockX, blockY, 'T', Block.BlockType.PIECE_WIN)
        //WHEN
        val upOne = block.getTopCoordinate(1)
        val upTwo = block.getTopCoordinate(2)

        val rightOne = block.getRightCoordinate(1)
        val rightTwo = block.getRightCoordinate(2)

        val downOne = block.getBottomCoordinate(1)
        val downTwo = block.getBottomCoordinate(2)

        val leftOne = block.getLeftCoordinate(1)
        val leftTwo = block.getLeftCoordinate(2)


        //THEN
        Assert.assertEquals("block up One", blockY - 1, upOne)
        Assert.assertEquals("block up Two", blockY - 2, upTwo)

        Assert.assertEquals("block right One", blockY + (blockWidth - 1) + 1, rightOne)
        Assert.assertEquals("block right Two", blockY + (blockWidth - 1) + 2, rightTwo)

        Assert.assertEquals("block down One", blockY + (blockHeight - 1) + 1, downOne)
        Assert.assertEquals("block down Two", blockY + (blockHeight - 1) + 2, downTwo)

        Assert.assertEquals("block left One", blockY - 1, leftOne)
        Assert.assertEquals("block left Two", blockY - 2, leftTwo)
    }
}