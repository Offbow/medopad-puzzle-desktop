package test.com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.Board
import test.com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BOARD
import test.com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BOARD_PRINTOUT
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import org.junit.Assert
import org.junit.Test

class TestBoardMapUtil() {

    @Test
    fun `Test Board Util Print-Format of Board Map`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val map = board.getMap()
        val printedMap = BoardMapUtil.formatBoardMap(map)
        //THEN
        Assert.assertEquals("Boards prints are Equal", VALID_BOARD_PRINTOUT, printedMap)
    }
}