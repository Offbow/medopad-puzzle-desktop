package test.com.medopad.puzzle.slidingpuzzle

import com.medopad.puzzle.slidingpuzzle.Board
import test.com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BASIC_BOARD
import test.com.medopad.puzzle.slidingpuzzle.TestConsts.VALID_BOARD
import com.medopad.puzzle.slidingpuzzle.model.GameMove
import com.medopad.puzzle.slidingpuzzle.solver.BoardMapUtil
import org.junit.Assert
import org.junit.Test

class TestBoard {

    @Test
    fun `Test Board map construction full`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val map = board.getMap()
        val printedMap = BoardMapUtil.formatBoardMap(map)
        //THEN
        Assert.assertEquals("Constructed Board Equal full", TestConsts.VALID_BOARD_PRINTOUT, printedMap)
    }

    @Test
    fun `Test Board map construction basic`() {
        //GIVEN
        val board = Board(VALID_BASIC_BOARD)
        //WHEN
        val map = board.getMap()
        val printedMap = BoardMapUtil.formatBoardMap(map)
        //THEN
        Assert.assertEquals("Constructed Board Equal basic", TestConsts.VALID_BASIC_BOARD_PRINTOUT, printedMap)
    }

    @Test
    fun `Test Board VALID move, Distance 1`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val moveResult = board.moveBlock(GameMove.DOWN, 1, 9)

        //THEN
        Assert.assertEquals("Movement is valid", Board.MoveResult.ALLOWED, moveResult)
    }

    @Test
    fun `Test Board VALID move, Distance 2`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val moveResult = board.moveBlock(GameMove.LEFT, 2, 11)
        //THEN
        Assert.assertEquals("Movement is valid", Board.MoveResult.ALLOWED, moveResult)
    }

    @Test
    fun `Test Board BLOCK move, Distance 1`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val moveResult = board.moveBlock(GameMove.UP, 1, 12)
        //THEN
        Assert.assertEquals("Movement is blocked", Board.MoveResult.BLOCKED, moveResult)
    }

    @Test
    fun `Test Board BLOCK move, Distance 2`() {
        //GIVEN
        val board = Board(VALID_BOARD)
        //WHEN
        val moveResult = board.moveBlock(GameMove.UP, 2, 12)
        //THEN
        Assert.assertEquals("Movement is blocked", Board.MoveResult.BLOCKED, moveResult)
    }

    @Test
    fun `Test Board WIN move`() {
        //GIVEN
        val board = Board(VALID_BASIC_BOARD)
        //WHEN
        val moveResult = board.moveBlock(GameMove.DOWN, 1, 12)
        //THEN
        Assert.assertEquals("Movement wins board", Board.MoveResult.WIN, moveResult)
    }
}